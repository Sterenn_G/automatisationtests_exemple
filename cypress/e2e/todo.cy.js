/// <reference types="cypress" />

describe('example todo app', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/todo')
  })

  //Ajout d'un context
  context('with a checked task', () => {
    beforeEach(() => {

      //Tester coche élément liste
      cy.contains('Pay electric bill')
     .parent()
     .find('input[type=checkbox]')
     .check()
    })

    it('can filter for uncompleted tasks', () => {
      cy.contains('Active').click()

      cy.get('.todo-list li')
        .should('have.length', 1)
        .first ()
        .should('have.text', 'Walk the dog')
    
      //Checker que cet élément existe (commenter si déjà supprimé)  
      cy.contains('Pay electric bill').should('not.exist',) 
      //cy.contains('Pay electric bill')
      //  .parents('li')
      //  .should('have.class', 'completed') 
    })
  })//fermeture contexte

  //Test 2eme items
  it ('displays two todo items by default', () => {
    cy.get('.todo-list li').should('have.length', 2)
  })

  //Test ajout d'un item
  it ('can add a new todo items', () => {
    const newItem = 'Feed the cat'
    cy.get ('[data-test=new-todo]').type(`${newItem} {enter}`)
    cy.get ('.todo-list li').should('have.length', 3)
    .last()
    .should('have.text', newItem)
  })

  //Mise à jour du footer
//   it ('count todo items', () => {
//     cy.get ('.todo-list li').should('have.length', 3)
//     .last()
//     .should('have.text', newItem)
//   })
})